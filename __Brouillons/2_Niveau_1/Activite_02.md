# Scénariser une activité

1. Choisir une activité existante (peut-être parmi celles du module [Penser Concevoir Elaborer](../../1_Penser-Concevoir-Elaborer)) et en donner un scénario possible.

2. Discuter différents scénarios pour une même activité.
