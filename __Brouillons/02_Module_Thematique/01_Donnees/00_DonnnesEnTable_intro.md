# Fiche thématique

## Données en tables

### [Le programme officiel](https://cache.media.eduscol.education.fr/file/SP1-MEN-22-1-2019/26/8/spe633_annexe_1063268.pdf)

| Contenus | Capacités attendues | Commentaires |
| -------- | ------------------- | ------------ |
Indexation de tables | Importer une table depuis un fichier texte tabulé ou un fichier CSV. | Est utilisé un tableau doublement indexé ou un tableau de p-uplets qui partagent les mêmes descripteurs. |
| Recherche dans une table | Rechercher les lignes d’une table vérifiant des critères exprimés en logique propositionnelle. | La recherche de doublons, les tests de cohérence d’une table sont présentés. |
| Tri d’une table | Trier une table suivant une colonne. | Une fonction de tri intégrée au système ou à une bibliothèque peut être utilisée. |
| Fusion de tables | Construire une nouvelle table en combinant les données de deux tables. | La notion de domaine de valeurs est mise en évidence. |

### Commentaire

Cette partie du programme est relativement _simple_, au sens où il n'y a pas d'ambiguité ni de difficulté à comprendre les attendus.

Lors du traitement de ces données, on pourra avoir une progression dans la difficulté :

- ouvrir la table avec un tableur et visualiser le contenu ; effectuer éventuellement une recherche simple
- faire constater sur une _grosse_ table que la manipulation n'est pas aisée : passer à un traitement via un langage de programmation semble donc naturel
- ouvrir une table via les fonctions de manipulation de fichiers avec Python ; effectuer une recherche simple
- effectuer un tri à l'aide des fonctions de tri de Python : `sort` et `sorted`
- effectuer la fusion de tables
