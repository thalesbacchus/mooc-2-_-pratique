Pour prendre du recul et nous auto-évaluer par rapport à la conférence d'Olivier, posons quelques questions à ces sujets :

Q1. Qu'est ce qui différencie [pédagogie](https://fr.wikipedia.org/wiki/P%C3%A9dagogie) et [didactique](https://fr.wikipedia.org/wiki/Didactique) finalement ? Pour chaque item essayons de le classer par rapport à l'une ou l'autre

- d- L'apprentissage de la programmation
- p- L'entrainement à la résolution de problème
- p- L'engagement de l'élève dans son apprentissage
- d- La compréhension de la notion de codage de l'information
- p- Les méthodes pour évaluer l'apprentissage de concept

Feedback : Effectivement on parlera de didactique pour un apprentissage disciplinaire en se focalisant sur la façon d'apprendre un savoir ou de permettre de maîtriser une compétence, les éléments pédagogiques sont plus centrés sur la personne apprenante que la discipline donc sont plus transversales.

Q2. Quand peut-on parler d'apprentissage de la [pensée informatique](https://interstices.info/la-pensee-informatique/) plutôt que d'apprentissage de l'informatique ?

- x Ça n'a pas de sens c'est une notion qui n'a jamais été définie
- x Si il y a bien une définition unique et univoque de la pensée informatique
- x La notion de pensée informatique a des definitions multiples selon de quelles disciplines on parle
- o Les compétences relatives à la pensée informatique englobent des savoir et savoir-faire impliquant des concepts informatiques mais transposables à d'autres domaines
- x On parle de pensée informatique pour décrire les pensées générées par des algorithmes dans les ordinateurs

Feedback : la pensée informatique et effectivement une notion bien défini comment l'explique dans les ressources complémentaires de la vidéo mais ce n'est pas une définition univoque elle est spécifique à l'informatique et il s'agit bien de prendre en compte des concepts qui sont transposables à d'autres domaines

Q3. Qu'est ce que le tracing ?

- x Un outil numérique pour mesurer et analyser les traces d'apprentissages permettant d'optimiser les performances en programmation
- o Le fait de simuler l'exécution d'un programme pas à pas sur papier, pour voir les changements de valeurs des variables et en comprendre le fonctionnement
- x Signifie apprendre en suivant les traces de la personne qui enseigne, où on scénarise des tatônnements, des erreurs, avant de trouver la bonne solution.  

Q4. Quel intérêt du pair à pair : pointer les arguments pédagogiques en faveur d'un travail en binôme sur une machine lors d'une activité incluant la programmation.

- x Ça fait deux fois moins d'ordinateurs, c'est plus économique et plus écologique
- o L'élève qui ne programme pas peut prendre du recul et apprendre à lire du code, pas juste l'écrire
- x C'est un mauvais choix pédagogique, cela fait baisser l'engagement de la moitié des élèves
- o Un·e élève qui a compris va pouvoir exprimer les choses de manière plus proche l'autre apprenant·e
- o Cela entraîne à reformuler sa pensée et la structurer pour consolider son apprentissage 

Sur les conseils pratiques ? 

- Reprendre les conseils pratiques et choisir (évaluation par partage sur la plateforme)
  - (i) un conseil qui vous semble particulièrement utile pour aller le partager sur le forum et expliquer comment le mettre en oeuvre, vous pourriez même en faire une capsule vidéo comme proposé ici https://cai.community/ressource/comment-creer-des-grains-videos-de-partage-de-bonne-pratique/ 
 - (ii) le conseil qui vous parle le moins ou qui vous semble le plus discutable et aller sur le forum en faire (de manière bienveillante ;) …) une analyse critique pour faire ensuite une contre-proposition; même si il y en a pas, en choisir un pour créer un dialogue dialectique thèse+anti-thèse=synthèse


