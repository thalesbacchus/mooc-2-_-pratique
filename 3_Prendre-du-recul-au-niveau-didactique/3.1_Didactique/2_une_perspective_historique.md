# Éléments de Didactique : une perspective historique

## Historique

Jacques Baudé, Président d'honneur de l'association Enseignement Public et Informatique, membre d'honneur de la Société Informatique de France nous offre un panorama [1] synthétique mais complet des différentes expérimentations et essais d'introduction de l'Informatique dans l'enseignement en France. 

On y apprend que dès les années 50, la France prend la mesure de l'importance de l'Informatique dans le monde économique et industriel. C'est la mise en place des premiers équipements et des premiers enseignements dans les universités et les instituts [2].

L'effort se poursuit dans les années 60 avec la création d'entreprises privées comme la <a href="https://fr.wikipedia.org/wiki/Compagnie_internationale_pour_l%27informatique" target="_blank">Compagnie internationale pour l'informatique</a> (CII) lors du <a href="https://fr.wikipedia.org/wiki/Plan_Calcul" target="_blank">Plan Calcul</a>. Cet engouement du monde économique se traduit par les premières expérimentations dans l'enseignement avec en point d'orgue, souvent considéré comme le début de l'enseignement de l'informatique, le séminaire de Sèvre [3].

A la suite de ce séminaire, apparaissent les premiers stages d'enseignants, stages dits _lourds_ d'un an dans des sociétés comme IBM, CII et Honeywell-Bull pour la première année puis dans des universités et instituts (IMAG de Grenoble, IUT de Nancy, Université de Rennes et de Toulouse, ENS de Saint-Cloud). Complétés par des stages plus légers (touchant plus de 6000 enseignants) ils constituent l'_expérience des 58 lycées_ entre 1970 et 1976 [4]. Pour les stages _lourds_, c'est environ 530 enseignants de toute discipline qui sont concernés. A noter que si les enseignants de mathématiques sont les plus représentés (140), les enseignants de Lettres (76) font jeu égal avec ceux de physique (75) et que le nombre d'enseignants de Langues (55) et même celui d'Histoire-Géo (42) surpasse celui des enseignants de Techniques Industrielles (22). 

De l'un de ces stages naît l'association Enseignement Public et Informatique (EPI) qui, dès son premier bulletin en décembre 1971 prône des **approches complémentaires** de l'Informatique :

- comme enseignement d'une nouvelle matière
- comme enseignement d'un méthode de pensée au sein des matières existantes
- comme outil dans la pratique quotidienne de l'enseignant

L'_expérience des 58 lycées_ a probablement marqué le glas d'une introduction de l'Informatique comme nouvelle discipline. Puisque dès le lancement, les objectifs affichés par le professeur Mercouroff, alors Chargé de Mission à l'Informatique au Ministère de l'Éducation Nationale, sont clairs :

> 1 - développer une formation de culture générale à l'informatique qui, aurait pour but « non pas d'apprendre l'informatique, mais d'apprendre que l'informatique existe, à quoi elle peut servir, ce qu'elle ne peut pas faire, quelles sont ses limites, quels sont les aspects économiques qui lui sont associés » ;
>
> 2 - favoriser par la même occasion une rénovation pédagogique en ouvrant l'enseignement secondaire sur le monde contemporain et en amenant les enseignants à « se poser des questions sur le contenu de leur enseignement ».
>
> La stratégie choisie pour atteindre simultanément ces deux objectifs a consisté à introduire l'informatique à travers les disciplines traditionnelles au lieu de créer une discipline nouvelle, avec ses horaires et ses programmes, qui aurait par ailleurs surchargé un enseignement déjà taxé d'encyclopédisme

A la suite de cela, l'enseignement de l'Informatique se fait discrète pendant près de 10 ans. Puis revient en 1985 avec le _Plan Informatique pour Tous_[5], fondé sur l'équipement massif des établissements publics d'enseignement et la généralisation d'une _option d'informatique_ créée en 1980 pour les lycées. Mais là encore c'est la vision _outil_ qui est privilégiée et malgré les efforts de certains[6] cet essai avorte deux fois en 1992 et 1998 [7]. Ce qui semble incompréhensible, compte tenu du succès du grand colloque _Informatique et Enseignement_ qui a eu lieu en 1983 sur proposition de l'EPI au ministre de l'éducation de l'époque et auquel assistera le Président Mitterand en personne.

C'est en 1988 qu'a lieu le premier _Colloque francophone pour la didactique de l'Informatique_ qui fera naître l'Association Francophone pour la Didactique de l'Informatique (AFDI) [8]. Quatre colloques internationaux sont à son initiative : Namur 1990, Sion 1992, Québec 1994, Monastir 1996.

Les années 90 ne font que confirmer la vision _outil dans les disciplines_ de l'Informatique par le ministère. En 2000 naît le _Brevet Informatique et Internet (B2i)_ censé attester la maîtrise de l'utilisation de l'outil informatique et d'internet par les élèves de collège. Axé sur des compétences sans connaissances précises à acquérir, ce brevet ne va jamais vraiment décoller. Une des raisons avancées était la non formation des enseignants eux-mêmes. En 2011 est alors créé le  _Certificat Informatique et Internet_ (C2i). En réalité il s'agit d'une collection de certificats :

- le C2i niveau 1 : généraliste s'obtient en Licence, 
- les C2i niveau 2 : spécialisés par domaine professionnel ; on y trouve le C2i2e : le C2i dédié au métier de l'enseignement et exigé en Master pour les futurs enseignants.

### Le tournant

En 2005 est organisé à l'Académie des Sciences un grand débat intitulé _L'enseignement de l'informatique de la maternelle à la terminale_. Y interviennent <a href="https://fr.wikipedia.org/wiki/G%C3%A9rard_Berry" target="_blank">Gérard Berry</a>, <a href="https://fr.wikipedia.org/wiki/Gilles_Dowek" target="_blank">Gilles Dowek</a> et <a href="https://fr.wikipedia.org/wiki/Maurice_Nivat" target="_blank">Maurice Nivat</a>. Tous les trois sont d'accord avec la petite phrase de M. Nivat tirée de son intervention :

> [...] il convient d'introduire au lycée un véritable enseignement d'informatique formant aux notions essentielles d'algorithme et de programme [...]

A partir de cette date, plusieurs actions et interventions convergent pour une prise de conscience de l'importance d'un enseignement de l'informatique chez nos décideurs. Les principaux acteurs sont : l'Académie des Sciences, le Conseil National du Numérique, l'INRIA, l'association de parents d'élèves PEEP, des Inspecteurs généraux et de nombreuses personnalités du monde informatique.

En 2009, le Ministre Chatel annonce la création d'une spécialité _Informatique et Science du Numérique (ISN)_ pour la BAC série S et deux ans après paraît au Bulletin Officiel le programme de cette spécialité [9] construit autour de quatre piliers de la discipline :

- représentation de l'information
- algorithmique
- langages et programmation
- architectures matérielles

Des formations universitaires commandées par les Rectorats se mettent en place partout en France.

Et les choses s'intensifient, dans son rapport _L'enseignement de l'informatique en France -- Il est urgent de ne plus attendre_[10] l'Académie des Sciences préconise un enseignement **non optionnel** assis sur l'introduction de la discipline informatique, dès le collège, au même titre que la physique ou la biologie. Ainsi, après les programmes de maternelle paru en 2015, ceux de cycles 2 et 3 en 2016, une option _Informatique et Création Numérique (ICN)_ voit le jour en 2016 et 2017 pour les classes de premières ES, L et S et Terminale ES et L [11].

Et cela nous conduit au présent de ce MOOC : la création de la spécialité _Numérique et Science Informatique_ en 2019, de la première session du CAPES NSI associé en 2020 et l'agrégation du même nom qui est sur les rails. Mais il reste du travail sur la didactique : les programmes d'enseignements [12] sont encore jeunes et demandent de la part des enseignants un certain recul pour être abordés et interprétés au mieux pour la transmission d'un savoir informatique riche et diversifié.


## Références

[1] _Éléments pour un historique de l'informatique dans l'enseignement général français. Sur sept décennies._ Jacques Baudé, EpiNet, Association Enseignement Public et Informatique, 2021, <a href="https://edutice.archives-ouvertes.fr/edutice-03161804" target="_blank">https://edutice.archives-ouvertes.fr/edutice-03161804</a>

[2] _Pour une histoire de l'informatique dans l'enseignement français - premiers jalons_, Émilien Pélisset,  Revue de l'EPI,  Association Enseignement Public et Informatique, 2002, <a href="https://edutice.archives-ouvertes.fr/edutice-00284085" target="_blank">https://edutice.archives-ouvertes.fr/edutice-00284085</a>

[3] Séminaire du Centre d'études et de recherches pour l'innovation dans l'enseignement (CERI de l'OCDE) au Centre national d'études pédagogiques de Sèvres en mars 1970, https://www.societe-informatique-de-france.fr/wp-content/uploads/2017/10/1024-no11-Baude.pdf

[4] _Expérience des 58 lycées_, menée entre 1970 et 1976 et qui, contrairement à ce que laisse entendre le nom concernait aussi le premier cyle et les collèges. L'évaluation de cette expérimentation : <a href="https://edutice.archives-ouvertes.fr/edutice-00560705/file/b23p068.htm" target="_blank">https://edutice.archives-ouvertes.fr/edutice-00560705/file/b23p068.htm</a>

[5] _plan Informatique Pour Tous (IPT)_, Jacques Baudé, Bulletin de la SIF 1024, n°5, mars 2015, https://www.societe-informatique-de-france.fr/wp-content/uploads/2015/04/1024-5-baude.pdf

[6] <a href="https://fr.wikipedia.org/wiki/Jacques_Arsac" target="_blank">Jacques Arsac</a>, professeur émérite d'Informatique de l'Université Pierre et Marie Curie, membre de l'Académie des Sciences, proposera une deuxième option d'informatique orientée sciences et notamment programmation, domaine où cet informaticien a publié plusieurs ouvrages.

[7] _L'option informatique des lycées dans les années 80 et 90 (3 parties)_, Jacques Baudé, EpiNet, oct-nov 2010, <a href="https://edutice.archives-ouvertes.fr/edutice-00564559/file/index.htm" target="_blank">https://edutice.archives-ouvertes.fr/edutice-00564559/file/index.htm</a>, https://www.societe-informatique-de-france.fr/wp-content/uploads/2014/02/1024-2-baude.pdf

[8] _Association Francophone pour la Didactique de l'Informatique_, association loi 1901, statuts déposés en juillet 1995, en sommeil mais non officiellement dissoute, <a href="https://www.epi.asso.fr/association/dossiers/epi-afdi.htm" target="_blank">https://www.epi.asso.fr/association/dossiers/epi-afdi.htm</a>

[9] Programme de la spécialité _Informatique et Science du Numérique_, Tle S, Bulletin officiel spécial n°8 du 13 octobre 2011, <a href="https://www.education.gouv.fr/bo/11/Special8/MENE1119484A.htm" target="_blank">https://www.education.gouv.fr/bo/11/Special8/MENE1119484A.htm</a>

[10] _L'enseignement de l'informatique en France -- Il est urgent de ne plus attendre_, Rapport de l'Académie des Sciences, mai 2013, <a href="https://www.academie-sciences.fr/pdf/rapport/rads_0513.pdf" target="_blank">https://www.academie-sciences.fr/pdf/rapport/rads_0513.pdf</a>

[11] Programme de l'option _Informatique et Création Numérique_, Bulletin officiel n°29 du 21 juillet 2016, <a href="https://www.education.gouv.fr/bo/16/Hebdo29/MENE1616734A.htm" target="_blank">https://www.education.gouv.fr/bo/16/Hebdo29/MENE1616734A.htm</a>

[12] Programmes de la spécialité _Numérique et Sciences Informatique_ des classes de Première et Terminale, Bulletins officiels n°1 du 11 janvier 2019 et n°8 du 25 juillet 2019, <a href="https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-g" target="_blank">https://eduscol.education.fr/2068/programmes-et-ressources-en-numerique-et-sciences-informatiques-voie-g</a>
