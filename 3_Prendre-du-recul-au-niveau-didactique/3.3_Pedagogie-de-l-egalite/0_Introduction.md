# Apprendre à enseigner l'égalité

Les recommandations qui incitent l’institution scolaire à œuvrer pour l’égalité des sexes ne manquent pas et, par exemple en France au niveau national, les <a href="https://www.education.gouv.fr/egalite-entre-les-filles-et-les-garcons-9047" target="_blank">initiatives se multiplient</a>. L’éducation à l’égalité des sexes fait ainsi son entrée à l’école. Pourtant, la manière dont elles sont mises en œuvre et leur éléments fondateurs posent question. Par ailleurs, au delà de la prise de conscience, nous avons besoin d'apprendre _concrètement_ à transformer les enseignements pour permettre que s'instaure durablement et de manière générale cette égalité. 

Oui mais comment ?

## Une approche à trois niveaux

Essayons de commencer, nous, qui faisons cette <a href="https://mooc-nsi-snt.gitlab.io/portail/" target="_blank">formation dite NSI</a> par être exemplaire; par parfait : juste donner l'exemple, au niveau de choix des collègues, de l'usage d'un <a href="https://fr.wikipedia.org/wiki/Langage_%C3%A9pic%C3%A8ne" target="_blank">langage épicène</a> sans "rejeter" l'écriture inclusive dont les effets bénéfiques ont été scientifiquement démontrés (Voir l'ouvrage « Le cerveau pense-t-il au masculin » de Pascal Gygax, Ute Gabriel, Sandrine Zufferey, Éditions Le Robert, 2021), et dans le présent fait que nous introduisons ce module dans la formation. Alors … aidez-nous ! Oui, si vous relevez quelque chose à corriger ou améliorer au sein même de ces ressources, faites nous [immédiatement un retour](mailto:classcode-accueil@inria.fr?subject=Un%20retour%20correctif%20à%20propos%20de%20la%20formation%20dite%20NSI&body=Bonjour,%0ADans%20cette%20page%20(lien)%20:%0AIl%20y%20a%20cet%20élément%20à%20corriger%20:%0ABien%20Cordialement). Votre vigilance sera formatrice pour vous et nous aidera beaucoup.

Nous proposons ensuite une réflexion sur le genre et les rapports sociaux de sexe à l’œuvre chez leurs élèves, afin qu’à notre tour, nous puissions y éveiller les élèves, en adoptant un positionnement de pédagogie féministe critique, positive et bienveillante. En clair, ne pas se positionner dans une posture de "lutte" contre les discriminations mais à celle d'œuvrer en faveur de l’égalité, sans aucune concession mais avec bienveillance, car nous avons tous et toutes besoin de progresser sur ce sujet.

Nous offrons de plus, au sein du travail fait ici de co-construction de ressources qui nous permet d'apprendre à enseigner l'informatique, à la fois des activités spécifiquement en lien avec cette pédagogie de l'égalité et un regard critique constructif sur toutes les activités par rapport à ce sujet.

Et nous évaluerons ensemble au fil du temps, puisque cette formation est aussi un accompagnement, à travers le forum probablement, comment il a été possible de mettre en œuvre tout cela, détecter les questions qui se posent en situation, et les réponses que nous pouvons y apporter.

Pour que nous formions bien les futur·e·s citoyennes et citoyen à l'informatique pour maîtriser le numérique, car pour le co-construire et pour en faire le meilleur, on a besoin des _deux_ moitiés de l'humanité.

