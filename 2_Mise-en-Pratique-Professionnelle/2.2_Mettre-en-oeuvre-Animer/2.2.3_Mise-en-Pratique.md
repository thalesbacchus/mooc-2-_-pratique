# À faire vous-même

Les objectifs de ce Module 2 sont :

1. Créer une analyse d'intention c'est-à-dire expliciter la mise en oeuvre de l'activité en classe ; comme celles présentées dans les exemples. Ce document pourra être un markdown ou un pdf, ou encore une vidéo (attention dans ce dernier cas à fournir un lien de type _streaming_ pour la visualisation de la ressource) 
2. Déposer ce fichier (s'il ne s'agit pas d'une vidéo) sur votre espace gitlab
3. Se rendre sur le Forum pour y mettre un _post_, dans la rubrique dédiée, pour annoncer le travail fait. D'autres pourront alors accéder à votre travail, le commenter, vous faire des retours
4. De votre côté, regardez ce que d'autres ont proposé, commentez, échangez avec les auteur-es.

